import processing.core.{PApplet, PConstants}
import processing.event.KeyEvent
import snake.graphics.Color
import snake.logic._

object Snake {
  def main(args:Array[String]): Unit = {
    PApplet.main("Snake")
  }
}



class Snake extends PApplet {
  val widthCellInPixels = 15
  val heightCellInPixels = widthCellInPixels
  val framesPerSecond = 10
  val updateTimer = new UpdateTimer(framesPerSecond)
  var gameState = new SnakeLogic()
  val onScreenGrid = OnScreenGrid(gameState.nrColumns,gameState.nrRows ,
    widthCellInPixels,heightCellInPixels)

  // this function is wrongly named draw by processing (is called on each update)
  override def draw(): Unit = {
    updateState()
    drawScreen()
  }

  def updateState() = {
    if(updateTimer.timeForNextFrame()) {
      gameState.step()
      updateTimer.advanceFrame()
    }
  }


  def drawScreen() : Unit = {
    if (gameState.isGameOver) drawGameOverScreen()
    else drawGrid()
  }


  def drawGrid() : Unit = {
    setBackground(Color.white)
    // for (y <- 0 until height; x <- 0 until width) {
    for (y <- 0 until gameState.nrRows; x <- 0 until gameState.nrColumns) {
      drawCell(x, y, gameState.getGridType(x, y))
    }
  }

  def drawCell(colIndex: Int, rowIndex: Int, cell: GridType): Unit = {
    cell match {
      case SnakeCell(p) => {
        val color = Color.lawnGreen.interpolate(p,Color.darkgreen)
        onScreenGrid.drawRectangleAt(colIndex, rowIndex, color)
      }
      case Food() => onScreenGrid.drawCircleAt(colIndex,rowIndex, Color.red)
      case Empty() =>
    }
  }


  def drawGameOverScreen() : Unit = {
    setBackground(Color.black)
    setFill(Color.red)
    textAlign(PConstants.CENTER, PConstants.CENTER)
    textSize(20)
    text("GAME OVER!", onScreenGrid.widthInPixels / 2, onScreenGrid.heigthInPixels / 2)
  }




  override def keyReleased(event: KeyEvent): Unit = {
    event.getKeyCode match {
      case java.awt.event.KeyEvent.VK_R => gameState.setReverseTime(false)
      case _ =>
    }
  }


  override def keyPressed(event: KeyEvent): Unit = {
    event.getKeyCode match {
      case java.awt.event.KeyEvent.VK_LEFT => gameState.changeDir(West())
      case java.awt.event.KeyEvent.VK_RIGHT => gameState.changeDir(East())
      case java.awt.event.KeyEvent.VK_UP => gameState.changeDir(North())
      case java.awt.event.KeyEvent.VK_DOWN => gameState.changeDir(South())
      case java.awt.event.KeyEvent.VK_R => gameState.setReverseTime(true)
      case _ =>
    }
  }


  class UpdateTimer(val framesPerSecond : Float) {
    val frameDuration : Float = 1000 / framesPerSecond
    var nextFrame = Float.MaxValue

    def init() = nextFrame = currentTime() + frameDuration
    def timeForNextFrame() : Boolean = currentTime() >= nextFrame

    def advanceFrame() = nextFrame = nextFrame + frameDuration
  }


  case class OnScreenGrid(
                           nrColumns : Int,
                           nrRows : Int,
                           widthCellInPixels : Int,
                           heightCellInPixels : Int) {
    val widthInPixels = nrColumns * widthCellInPixels

    val heigthInPixels = nrRows * heightCellInPixels

    def drawRectangleAt(col : Int, row : Int, color : Color) = {
      val x = col * widthCellInPixels
      val y = row * heightCellInPixels
      setFill(color)
      rect(x, y, widthCellInPixels, heightCellInPixels)
    }

    def drawCircleAt(col : Int, row : Int, color : Color) : Unit = {
      val x = col * widthCellInPixels
      val y = row * heightCellInPixels
      setFill(color)
      ellipse(x + widthCellInPixels / 2, y + heightCellInPixels / 2,
        widthCellInPixels, heightCellInPixels)
    }

  }

  // a rename for the obscurely named function millis()
  // gives the current time in milliseconds since
  // starting the program
  def currentTime() : Int  = millis()



  // better interfaces, processing has unclear names
  def setFill(c : Color) : Unit = {
    fill(c.red,c.green,c.blue,c.alpha)
  }

  // better interfaces, processing has unclear names
  def setBackground(c : Color) : Unit = {
    background(c.red,c.green,c.blue,c.alpha)
  }
  override def settings(): Unit = {
    size(onScreenGrid.widthInPixels, onScreenGrid.heigthInPixels, PConstants.P2D)
  }

  override def setup() : Unit = {
    updateTimer.init()
  }

}
