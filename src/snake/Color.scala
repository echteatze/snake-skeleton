package snake

package graphics

case class Color(red : Float, green : Float, blue : Float, alpha : Float) {

  // this is called on new Color(r,g,b)
  def this(red : Float, green : Float, blue : Float) = this(red,green,blue,255)

  def linearInterpolation(l : Float, r : Float, t : Float) = (1-t) * l + t * r

  def interpolate(fraction : Float, rhs : Color) : Color =
    Color(linearInterpolation(red,rhs.red, fraction),
      linearInterpolation(green,rhs.green, fraction),
      linearInterpolation(blue,rhs.blue, fraction),
      linearInterpolation(alpha,rhs.alpha, fraction)
    )

}

// companion object
object Color {

  // this is called on Color(r,g,b) (without new)
  def apply(red : Float, green : Float, blue : Float) : Color = new Color(red,green,blue)

  val lawnGreen = Color(124,252,0)
  val darkgreen = Color(0,100,0)
  val black = Color(0,0,0)
  val red = Color(255,0,0)
  val white = Color(255,255,255)

}
