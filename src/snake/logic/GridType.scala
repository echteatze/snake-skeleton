package snake.logic

sealed abstract class GridType
// 0 is head, 1.0 is tail
case class SnakeCell(headPercentage : Float ) extends GridType
case class Empty() extends GridType
case class Food() extends GridType