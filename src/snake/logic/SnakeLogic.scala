package snake.logic

// Your code goes in this file

class SnakeLogic(val nrColumns : Int, val nrRows : Int) {

  def this() = this(SnakeLogic.defaultColumns,SnakeLogic.defaultRows)

  def isGameOver() = false

  def step() : Unit = {}


  def changeDir(d : Direction) : Unit = {}

  def getGridType(x : Int, y : Int) : GridType = Empty()


  def setReverseTime(reverse : Boolean ) = {
    // BONUS:
  }

}

// companion object
object SnakeLogic {
  val defaultColumns = 25
  val defaultRows = 25
}

